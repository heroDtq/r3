package r3.flow;

import java.lang.reflect.Method;

/**
 * InHandler
 *
 * @author zhoufn
 * @create 2018-01-03 15:50
 **/
public abstract class InHandler {

    public Object in(Method method,Object[] results) throws Throwable {
        if(method.getReturnType().equals(void.class)){
            return null;
        }else{
            return this.join(results);
        }
    }

    /**
     * 自定义多节点返回参数的合并
     * @param results
     * @return
     * @throws Throwable
     */
    public abstract Object join(Object[] results) throws Throwable;

}
