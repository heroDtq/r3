package r3.rpc.server;

import org.springframework.context.ApplicationContext;
import r3.rpc.InvokePolicy;

import java.util.concurrent.CountDownLatch;

/**
 * Rpc server
 */
public interface Server {


    /**
     * start
     */
    void start(CountDownLatch downLatch);

    /**
     * stop
     */
    void stop();

    /**
     * 获取反射策略
     * @return
     */
    Class<? extends InvokePolicy> getInvokePolicyClass();

}
