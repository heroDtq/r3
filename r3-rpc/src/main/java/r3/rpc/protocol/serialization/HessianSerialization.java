package r3.rpc.protocol.serialization;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * HessianSerialization
 *
 * @author zhoufn
 * @create 2017-12-25 11:04
 **/
public class HessianSerialization implements Serialization {
    /**
     * 将对象进行序列化
     *
     * @param object
     * @return
     */
    @Override
    public byte[] serialize(Object object) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Hessian2Output out = new Hessian2Output(bos);
        byte[] data = null;
        try {
            out.startMessage();
            out.writeInt(1);
            out.writeObject(object);
            out.completeMessage();
            out.close();
            data = bos.toByteArray();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return data;
    }

    /**
     * 反序列化
     *
     * @param bytes
     * @param clazz
     * @return
     */
    @Override
    public <T> T deserialize(byte[] bytes, Class<T> clazz) {
        ByteArrayInputStream bin = new ByteArrayInputStream(bytes);
        Hessian2Input in = new Hessian2Input(bin);
        ArrayList list = new ArrayList();
        try {
            in.startMessage();
            int length = in.readInt();
            for (int i = 0; i < length; i++) {
                list.add(in.readObject());
            }
            in.completeMessage();
            in.close();
            bin.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Object o = list.get(0);
        if (o == null) {
            return null;
        }
        Class<?> aClass = o.getClass();
        if (aClass != clazz) {
            return null;
        }
        return (T) o;
    }
}
