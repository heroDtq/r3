package r3.cluster.router;

import r3.common.R3Url;
import r3.rpc.RpcRequest;
import r3.rpc.RpcResponse;

import java.util.List;

/**
 * 路由信息
 */
public interface Router {

    List<RpcResponse> route(List<RouterEntity> entities) throws Exception;

}
