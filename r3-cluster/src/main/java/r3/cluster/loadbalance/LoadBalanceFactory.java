package r3.cluster.loadbalance;

/**
 * LoadBalanceFactory
 *
 * @author zhoufn
 * @create 2017-12-28 11:45
 **/
public class LoadBalanceFactory {

    /**
     * @param loadBalance
     * @return
     */
    public static LoadBalance creatLoadBalance(String loadBalance){
        if("random".equalsIgnoreCase(loadBalance)){
            return new RandomLoadBalance();
        }
        return null;
    }

}
