package r3.cluster.loadbalance;

import r3.common.R3Url;

import java.util.HashMap;
import java.util.List;

/**
 * 软负载
 */
public interface LoadBalance {

    /**
     * @param worksSnapshot 内为当前可用服务的组列表，需要从每个组内选取一个可用服务；
     * @return 跨组可用服务列表
     * @throws Exception
     */
    List<R3Url> select(HashMap<String,HashMap<String,R3Url>> worksSnapshot) throws Exception;

}
