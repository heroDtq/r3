package r3.common;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * R3Url
 *
 * @author zhoufn
 * @create 2017-12-26 13:24
 **/
public class R3Url {

    @Setter
    @Getter
    private String namespace;

    @Setter
    @Getter
    private String interfce;

    @Setter
    @Getter
    private String applicaionName;

    @Setter
    @Getter
    private String beanId;

    @Setter
    @Getter
    private String applicationHost;

    @Setter
    @Getter
    private int applicationPort;

    /**
     * convert R3Url
     * @param url
     * @return
     */
    public static R3Url parse(String url){
        String[] strings = url.split("/");
        List<String> tempList = new ArrayList<>();
        for(String s : strings){
            if(StringUtils.isBlank(s)){
                continue;
            }
            tempList.add(s);
        }
        R3Url r3Url = new R3Url();
        r3Url.setInterfce(tempList.get(0));
        r3Url.setApplicaionName(tempList.get(2));
        r3Url.setBeanId(tempList.get(3).split("@")[0].trim());
        r3Url.setApplicationHost(tempList.get(3).split("@")[1].trim().split(":")[0].trim());
        r3Url.setApplicationPort(Integer.parseInt(tempList.get(3).split("@")[1].trim().split(":")[1].trim()));
        return r3Url;
    }

    public static void main(String[] args) {
        R3Url url = parse("/r3.example.api.SayHelloService/workers/worker-1/sayHelloWorker@192.168.2.173:20080");
        System.out.println(url);
    }
}
