package r3.common;

import java.net.InetAddress;
import java.util.UUID;

/**
 * R3Utils
 *
 * @author zhoufn
 * @create 2017-12-25 14:07
 **/
public class R3Utils {

    public static String randomUUID(){
        return UUID.randomUUID().toString().replace("-","");
    }

    public static String getNetIP() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        }catch (Exception e){
            throw new RuntimeException("Illegal IP...");
        }
    }

    public static void main(String[] args) throws Exception{
        System.out.println(getNetIP());
    }

}
